package com.cts.demos.ds.search;

public class BinarySearchDemo {

	public static void main(String[] args) {
		int intArr[] = { 5, 22, 24, 36, 39, 42, 57, 71, 88, 93 };
		int x = 88;
		int midIndex = intArr.length / 2;
		int index = -1;
		System.out.println("Index : " + index + " Value : " + intArr[index]);
	}

	private static int binarySearch(int[] intArray, int midIndex, int x) {

		if (x == intArray[midIndex]) {
			return midIndex;
		} else if (x > intArray[midIndex]) {
			midIndex = (midIndex + intArray.length) / 2;
			return binarySearch(intArray, midIndex, x);
		}else if (x < intArray[midIndex]) {
			midIndex = (midIndex + intArray.length) / 2;
			return binarySearch(intArray, midIndex, x);
		}

		return -1;
	}
}
