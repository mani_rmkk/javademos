package com.cts.demos.ds.search;

public class LinearSearchDemo {

	public static void main(String[] args) {
		int intArr[] = {31,22,63,24,5};
		int x = 24;
		int index = -1;
		for (int i = 0; i < intArr.length; i++) {
			if (x==intArr[i]) {
				index = i;
			}
		}
		
		System.out.println("Index : "+ index+" Value : "+intArr[index]);

	}

}
