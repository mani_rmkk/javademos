package com.cts.demos.reflet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RefletDemo {

	public static void main(String[] args) {
		try {
			Class employee = Class.forName("com.cts.demos.reflet.Employee");
			Method[] methods = employee.getMethods();
			Object emp = employee.newInstance();
			for (Method method : methods) {
				switch (method.getName()) {
				case "setName":
					method.invoke(emp, "Mani");
					break;
				case "setMobile":
					method.invoke(emp, 123456);
					break;

				default:
					break;
				}
			}
			System.out.println(emp);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
