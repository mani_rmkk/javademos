package com.cts.demos.lang;

import java.util.Date;

public class Employee {

	private Integer id;
	private String name;
	private Date join;

	public Employee(Integer id, String name, Date join) {
		super();
		this.id = id;
		this.name = name;
		this.join = join;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getJoin() {
		return join;
	}

	public void setJoin(Date join) {
		this.join = join;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", join=" + join + "]";
	}

}
