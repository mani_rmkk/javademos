package com.cts.demos.lang;

public class ReverseString {

	public static void main(String[] args) {
		String string = "ABC DEFG HIJK";
		char[] chars = string.toCharArray();
		int right = chars.length - 1;
		System.out.println(chars);
		for (int left = 0; left < right; left++, right--) {
			char temp = chars[left];
			chars[left] = chars[right];
			chars[right] = temp;
		}
		System.out.println(chars);		
	}
}
