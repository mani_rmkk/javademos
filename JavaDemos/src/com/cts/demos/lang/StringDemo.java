package com.cts.demos.lang;

public class StringDemo {

	public static void main(String[] args) {
		String s1 = "String1";
		String s2 = new String("String2");
		String s3 = "String1";
		String s4 = new String("String1");
		String s5 = s4;
		System.out.println(s1.compareTo(s2));
		System.out.println(s3 == s4);
		System.out.println(s3.equals(s4));
		System.out.println(s4 == s5);
	}
}
