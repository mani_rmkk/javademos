package com.cts.demos.lang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class CompDemo {

	public static void main(String[] args) {
		Employee employee1 = new Employee(1, "One", new Date());
		Employee employee2 = new Employee(2, "Two", new Date());
		Employee employee3 = new Employee(3, "Three", new Date());
		Employee employee4 = new Employee(4, "Four", new Date());
		
		ArrayList<Employee> employees = new ArrayList<Employee>();
		employees.add(employee3);
		employees.add(employee1);
		employees.add(employee4);
		employees.add(employee2);
		System.out.println(employees);
		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		System.out.println(employees);
		
	}
}
