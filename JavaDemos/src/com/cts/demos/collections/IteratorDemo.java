package com.cts.demos.collections;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class IteratorDemo {

	public static void main(String[] args) {
		Vector<String> strings = new Vector<String>();
		strings.add("One");
		strings.add("Two");
		strings.add("Three");
		Enumeration<String> enumeration = strings.elements();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			System.out.println(string);
			//strings.add("Foure");
		}
		Iterator<String> iterator = strings.iterator();
		while (iterator.hasNext()) {
			String string = (String) iterator.next();
			System.out.println(string);
			strings.add("Four");
		}
		
	}
}
