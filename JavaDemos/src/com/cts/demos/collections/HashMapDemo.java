package com.cts.demos.collections;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo {

	public static void main(String[] args) {
		HashMap<Employee, String> map = new HashMap<Employee, String>();
		
		map.put(new Employee(1, "One"), "One");
		map.put(new Employee(1, "One"), "One");
		map.put(new Employee(1, "One"), "One");
		map.put(new Employee(3, "Three"), "Three");
		map.put(new Employee(4, "Four"), "Four");
		
		System.out.println(map);
	}
}
