package com.cts.demos.threads;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable{

	private BlockingQueue<Message> blockingQueue;

	public Producer(BlockingQueue<Message> blockingQueue) {
		super();
		this.blockingQueue = blockingQueue;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			Message msg = new Message(new Date(), ""+i);
            try {
                Thread.sleep(i);
                blockingQueue.put(msg);
                System.out.println("Produced "+msg.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
		}
		//adding exit message
        Message msg = new Message(new Date(), "exit");
        try {
        	blockingQueue.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}
	
}
