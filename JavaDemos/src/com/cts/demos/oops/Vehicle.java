package com.cts.demos.oops;

public class Vehicle {

	private static String name;
	private int price;

	public Vehicle() {
		System.out.println("Vehicle Constructor Called");
	}
	public Vehicle(int price) {
		super();
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Vehicle.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Vehicle [name=" + name + ", price=" + price + "]";
	}

	public static String getVehicleNamePrice(){
		return name;
	}
}
