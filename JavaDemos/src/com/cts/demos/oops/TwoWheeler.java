package com.cts.demos.oops;

public class TwoWheeler extends Vehicle {

	public TwoWheeler(int price) {
		super(price);
		// TODO Auto-generated constructor stub
	}

	private int noofwheels;

	
	public TwoWheeler() {
		System.out.println("TwoWheeler Constructor Called");
	}
	public int getNoofwheels() {
		return noofwheels;
	}

	public void setNoofwheels(int noofwheels) {
		this.noofwheels = noofwheels;
	}
	
	public String getName() {
		return "Ovverriden : Name";
	}
	@Override
	public String toString() {
		return "TwoWheeler [noofwheels=" + noofwheels + ", getName()=" + getName() + ", getPrice()=" + getPrice() + "]";
	}
	
	public static String getVehicleNamePrice(){
		return "Ovverriden";
	}
}
