package com.cts.demos.oops;

public class InheritanceDemo {

	public static void main(String[] args) {
		Vehicle vehicle = new TwoWheeler();
		vehicle.setName("Pulser");
		vehicle.setPrice(12345);
		System.out.println(vehicle);
		System.out.println(Vehicle.getVehicleNamePrice());
		TwoWheeler twoWheeler = new TwoWheeler();
		twoWheeler.setName("Unicron");
		twoWheeler.setPrice(11111);
		System.out.println(twoWheeler);
		System.out.println(TwoWheeler.getVehicleNamePrice());
	}
}
