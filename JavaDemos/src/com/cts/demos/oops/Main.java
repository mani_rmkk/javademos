package com.cts.demos.oops;

public class Main {
	public static void main(String args[]) {
		A a = new B();
		B b = new B();
		a.fun(); // prints A.fun()
		b.fun();
	}
}
