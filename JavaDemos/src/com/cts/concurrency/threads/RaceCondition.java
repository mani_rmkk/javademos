package com.cts.concurrency.threads;

public class RaceCondition {

	public static void main(String[] args) throws InterruptedException {

		LongWrapper longWrapper = new LongWrapper();

		Runnable r = () -> {
			for (int i = 0; i < 10000; i++) {
				longWrapper.incrementValue();
			}
		};

		Thread[] threads = new Thread[10000];
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(r);
			threads[i].start();
		}
		
		for (int i = 0; i < threads.length; i++) {
			threads[i].join();
		}

		System.out.println("Value : " + longWrapper.getL());
	}
}
