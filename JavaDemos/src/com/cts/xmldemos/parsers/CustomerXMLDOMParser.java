package com.cts.xmldemos.parsers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cts.xmldemos.pojo.Address;
import com.cts.xmldemos.pojo.Customer;

public class CustomerXMLDOMParser {

	public static void main(String[] args) throws Exception {
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("./customers.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(inputStream);

		NodeList customerNodeList = document.getElementsByTagName("customer");

		List<Customer> customers = new ArrayList<Customer>();

		for (int i = 0; i < customerNodeList.getLength(); i++) {
			Node customerNode = customerNodeList.item(i);
			customers.add(buildCustomer(customerNode));
		}

		for (Customer customer : customers) {
			System.out.println(customer);
		}
	}

	private static Customer buildCustomer(Node customerNode) {
		Customer customer = new Customer();
		NamedNodeMap namedNodeMap = customerNode.getAttributes();
		Attr attr = (Attr) namedNodeMap.getNamedItem("id");
		customer.setId(Long.parseLong(attr.getValue()));
		NodeList customerDataNodeList = customerNode.getChildNodes();
		for (int j = 0; j < customerDataNodeList.getLength(); j++) {
			Node dataNode = customerDataNodeList.item(j);
			if (dataNode instanceof Element) {
				Element dataElement = (Element) dataNode;
				boolean isAddressNode = false;
				switch (dataElement.getTagName()) {
				case "id":
					customer.setId(Long.parseLong(dataElement.getTextContent()));
					break;
				case "firstName":
					customer.setFirstName(dataElement.getTextContent());
					break;
				case "lastName":
					customer.setLastName(dataElement.getTextContent());
					break;
				case "addresses":
					isAddressNode = true;
					break;

				default:
					break;
				}
				
				if (isAddressNode) {
					NodeList addressNodeList = dataElement.getChildNodes();
					for (int i = 0; i < addressNodeList.getLength(); i++) {
						Node addressNode = addressNodeList.item(i);
						customer.getAddresses().add(buildAddress(addressNode));
					}
				}
			}
		}
		return customer;
	}

	private static Address buildAddress(Node addressNode) {
		Address address = new Address();
		NodeList addressNodeList = addressNode.getChildNodes();

		for (int i = 0; i < addressNodeList.getLength(); i++) {
			Node node = addressNodeList.item(i);
			if (node instanceof Element) {
				Element element = (Element) node;

				switch (element.getTagName()) {
				case "type":
					address.setAddressType(element.getTextContent());
					break;
				case "street":
					address.setStreet1(element.getTextContent());
					break;
				case "city":
					address.setCity(element.getTextContent());
					break;
				case "state":
					address.setState(element.getTextContent());
					break;

				default:
					break;
				}
			}
		}

		return address;
	}

}
