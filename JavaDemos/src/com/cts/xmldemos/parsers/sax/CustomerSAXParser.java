package com.cts.xmldemos.parsers.sax;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class CustomerSAXParser {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		InputStream is = ClassLoader.getSystemResourceAsStream("./customers.xml");
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		SAXParser parser = factory.newSAXParser();
		CustomerSAXHandler handler = new CustomerSAXHandler();
		parser.parse(is, handler);
		System.out.println(handler.getCustomers());
	}
}
