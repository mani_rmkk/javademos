package com.cts.xmldemos.parsers.sax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.cts.xmldemos.pojo.Customer;

public class CustomerSAXHandler extends DefaultHandler {

	private List<Customer> customers = new ArrayList<Customer>();

	private Customer customer = null;

	private String nodeName = null;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		switch (localName) {
		case "customer":
			customer = new Customer();
			customer.setId(Long.parseLong(attributes.getValue("id")));
			break;
		case "firstName":
			nodeName = "firstName";
			break;
		case "lastName":
			nodeName = "lastName";
			break;
		case "email":
			nodeName = "email";
			break;
		case "addresses":
			System.out.println("Address Present");
			break;

		default:
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName.equals("customer")) {
			customers.add(customer);
			customer = null;
		} else if (nodeName != null) {
			nodeName = null;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (nodeName != null) {
			switch (nodeName) {
			case "firstName":
				customer.setFirstName(new String(ch, start, length));
				break;
			case "lastName":
				customer.setLastName(new String(ch, start, length));
				break;
			default:
				break;
			}
		}
	}

	public List<Customer> getCustomers() {
		return customers;
	}

}
